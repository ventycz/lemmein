export type Token = {
    issuer: string
    account: string
    code: string
}

export type PongMessage = {
    type: 'pong'
}

export type UpdateMessage = {
    type: 'update'
    tokens: Token[]
    time: number
}

export type ServerMessage = PongMessage|UpdateMessage;
