export type PingMessage = {
    type: 'ping'
}

export type RequestCertificateMessage = {
    type: 'request-certificate'
}

export type ClientMessage = PingMessage|RequestCertificateMessage|{type: never};
