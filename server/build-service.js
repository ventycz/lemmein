#!/usr/bin/node
//@ts-check
const { normalize } = require('path');

/**
 * @param {string} dir
 */
function buildService(dir) {
    dir = dir.replace(/\/$/, '');

    return (`
        [Unit]
        Description=LemmeIn Server
        After=network.target

        [Service]
        Environment=PORT=9091
        Type=simple
        ExecStart=/usr/bin/node ${dir}/server/dist/main.js
        WorkingDirectory=${dir}
        Restart=on-failure
        StandardOutput=journal
        StandardError=journal

        [Install]
        WantedBy=multi-user.target
    `).trim().replace(/^ {8}/gm, '');
}

let dir = normalize(`${__dirname}/../`);
const serviceContents = buildService(dir);

console.log(serviceContents);
