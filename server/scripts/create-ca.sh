#!/bin/bash
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
KEYS_DIR="$SCRIPT_DIR/../keys"

YEARS=5

# Generate a key pair
openssl genrsa -out "$KEYS_DIR/ca.key" 2048

# Use the private key from the key pair to generate a CA certificate.
openssl req -x509 -new -nodes -key "$KEYS_DIR/ca.key" -sha256 -days $((365 * $YEARS)) -subj "/CN=LemmeIn" -out "$KEYS_DIR/ca.pem"
