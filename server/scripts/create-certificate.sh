#!/bin/bash
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
KEYS_DIR="$SCRIPT_DIR/../keys"

YEARS=1
CN=$1

if [ -z "$CN" ]; then
    echo "Argument Name is missing"
    exit
fi

NAME="client_$CN"

# Generate a key pair
openssl genrsa -out "$KEYS_DIR/$NAME.key" 2048

# Create a CSR for the client certificate
openssl req -new -subj "/CN=$CN" -key "$KEYS_DIR/$NAME.key" -out "$KEYS_DIR/$NAME.csr"

# Create a client certificate from the CSR
openssl x509 -req -in "$KEYS_DIR/$NAME.csr" -CA "$KEYS_DIR/ca.pem" -CAkey "$KEYS_DIR/ca.key" -CAcreateserial -out "$KEYS_DIR/$NAME.pem" -days $((365 * $YEARS)) -sha256

# Create PFX version
openssl pkcs12 -inkey "$KEYS_DIR/$NAME.key" -in "$KEYS_DIR/$NAME.pem" -export -out "$KEYS_DIR/$NAME.pfx" -passout pass:
