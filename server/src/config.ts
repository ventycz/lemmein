import { readFile } from 'fs/promises';
import { resolve } from 'path';

import yaml from 'yaml';
import { watch } from 'chokidar';

export class Config {
    secrets: SecretsFile

    constructor() {
        this.secrets = new SecretsFile('secrets.yml');
    }
}

export class ConfigFile {
    private _path: string;
    private _doc: yaml.Document|null = null;
    private _toSave: any;

    constructor(path: string) {
        this._path = resolve(__dirname, '../config', path);

        let watcher = watch(this._path);
        watcher.on('change', async () => {
            console.log('Config file changed, reloading ... (%s)', this._path);
            await this.load(true);
        });
    }

    /**
     * Absolute path to the config file
     */
    get path() {
        return this._path;
    }

    /**
     * Load file contents
     *
     * @param force  Force load - loads the file even if it was loaded already
     */
    async load(force = false): Promise<void> {
        if (!force && this._doc) return;

        let contents = await readFile(this._path, { encoding: 'utf8' });
        this._doc = yaml.parseDocument(contents);
    }

    async save(): Promise<void> {
        if (!this._doc) throw new Error('Call load() first!');
        if (!this._toSave) throw new Error('Call set() first!');

        this._doc.contents = this._toSave;
    }

    /**
     * Get contents of file (will call load() if not done previously)
     */
    async get(): Promise<any> {
        if (!this._doc) await this.load();

        return this._doc!.toJSON();
    }

    /**
     * Sets contents for saving later
     */
    set(newContents: any) {
        this._toSave = newContents;
    }
}

export type Secret = {
    issuer: string
    account: string
    key: string

    type?: 'totp'
    algorithm?: 'SHA1'

    /**
     * Number of digits generated
     */
    digits?: number // default: 6

    /**
     * Refresh period
     */
    period?: number // default: 30
}

export class SecretsFile extends ConfigFile {
    get() {
        return super.get() as Promise<Secret[]>;
    }

    set(newContents: Secret[]) {
        return super.set(newContents);
    }
}