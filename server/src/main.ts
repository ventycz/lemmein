import uWS from 'uWebSockets.js';
import { authenticator } from 'otplib';
import { Config } from './config';
import { UpdateMessage, ClientMessage } from '@lemmein/shared';
import { TextDecoder } from 'util';
import { X509Certificate } from 'crypto';
import { readFileSync } from 'fs';
import { nanoid } from 'nanoid';

const INSECURE = +(process.env.INSECURE ?? 0) > 0;

class Server {
    private _app: uWS.TemplatedApp;
    private _caCert?: X509Certificate;
    private _dec = new TextDecoder('utf-8');
    private _cfg = new Config();
    private _lastTick: number|null = null;

    private _certRequests = new Map<string, Buffer>;

    constructor() {
        this._app = uWS.App();

        if (!INSECURE) {
            this._caCert = new X509Certificate(
                readFileSync(`${__dirname}/../keys/ca.pem`, 'utf-8')
            );
        } else {
            console.warn('Running in insecure mode!');
        }

        this._hookWs();
        this._setupTick();
        this._boot();
    }

    _decode(buffer: ArrayBuffer): string {
        return this._dec.decode(buffer);
    }

    _checkCertificate(req: uWS.HttpRequest) {
        const encoded = req.getHeader('x-ssl-cert');
        if (!encoded?.length) {
            return false;
        }

        let x509: X509Certificate;

        try {
            x509 = new X509Certificate(
                decodeURIComponent(encoded)
            );
        } catch (error) {
            return false;
        }

        if (!x509.checkIssued(this._caCert!)) {
            return false;
        }

        console.log('Processed request for serial:%s (%s)', x509.serialNumber, x509.subject);
        return true; // TODO: Certificate data
    }

    _getCertificate(req: uWS.HttpRequest) {
        const encoded = req.getHeader('x-ssl-cert');
        if (!encoded?.length) {
            return false;
        }

        return decodeURIComponent(encoded);
    }

    _hookWs() {
        this._app.get('/auth', (res, req) => {
            const result = this._checkCertificate(req);
            res.end(result.toString());
        });

        this._app.get('/get-certificate/:key', (res, req) => {
            const key = req.getParameter(0);
            if (!this._certRequests.has(key)) {
                res.writeStatus('400').end();
                return;
            }

            // Download headers
            res.writeHeader('Content-Disposition', `attachment; filename="lemmein-key.pfx"`);

            res.end(
                this._certRequests.get(key)!
            );
        });

        this._app.ws<{ cert: string }>('/ws', {
            upgrade: (res, req, context) => {
                if (this._caCert && !this._checkCertificate(req)) {
                    res.writeStatus('401').end();
                    return;
                }

                res.upgrade(
                    {
                        cert: this._getCertificate(req),
                    },
                    req.getHeader('sec-websocket-key'),
                    req.getHeader('sec-websocket-protocol'),
                    req.getHeader('sec-websocket-extensions'),
                    context
                );
            },

            open: (ws) => {
                // Subscribe to future updates
                ws.subscribe('code-update');

                // Send the first update message
                this._buildUpdateMessage().then(msg => {
                    ws.send(JSON.stringify(msg));
                });
            },

            message: (ws, message) => {
                try {
                    let decoded = this._decode(message);
                    let parsed = JSON.parse(decoded);

                    this._onWsMessage(ws, parsed);
                } catch (e) {
                    console.warn('Failed to process message:', (e as Error).message);
                }
            }
        });

        this._app.any('/*', (res, req) => {
            if (this._caCert && !this._checkCertificate(req)) {
                res.writeStatus('401').end();
                return;
            }

            res.writeStatus('404').end('Nothing to see here!');
        });
    }

    async _onWsMessage(ws: uWS.WebSocket<{ cert: string }>, message: ClientMessage): Promise<void> {
        // Send PONG response
        if (message.type === 'ping') {
            ws.send(JSON.stringify({ type: 'pong' }));

            return;
        }

        // Requesting the used certificate
        if (message.type === 'request-certificate') {
            const x509 = new X509Certificate(ws.getUserData().cert);
            const subject = x509.subject.replace(/^CN=/, '');

            let certificate;
            try {
                certificate = readFileSync(`${__dirname}/../keys/client_${subject}.pfx`);
            } catch (e) {
                ws.send(JSON.stringify({
                    type: 'certificate',
                    status: false,
                }));

                return;
            }

            const key = nanoid();
            this._certRequests.set(key, certificate);

            const timeoutSeconds = 60;

            // 60 seconds
            setTimeout(() => {
                this._certRequests.delete(key);
            }, timeoutSeconds * 1_000);

            // Send info to client
            ws.send(JSON.stringify({
                type: 'certificate',
                key,
                url: `/get-certificate/${key}`,
                timeoutSeconds,
            }));

            return;
        }

        console.log(message, this._decode(ws.getRemoteAddressAsText()));
    }

    async _buildUpdateMessage(): Promise<UpdateMessage> {
        let msg: UpdateMessage = {
            type: 'update',
            tokens: [],
            time: authenticator.timeRemaining()
        };

        const secrets = await this._cfg.secrets.get();

        for (let secret of secrets) {
            const token = authenticator.generate(secret.key);

            msg.tokens.push({
                issuer: secret.issuer,
                account: secret.account,
                code: token
            });
        }

        // Order tokens
        msg.tokens.sort((a, b) => {
            let aComp = `${a.issuer}-${a.account}`;
            let bComp = `${b.issuer}-${b.account}`;

            return aComp.localeCompare(bComp);
        });

        return msg;
    }

    async _sendUpdate() {
        let update = await this._buildUpdateMessage();
        this._app.publish('code-update', JSON.stringify(update));
    }

    async _setupTick() {
        // Run initial tick
        this._tick();

        // Run tick every 100ms (can be even faster)
        setInterval(() => this._tick(), 100);
    }

    async _tick() {
        let rem = authenticator.timeRemaining();

        // Already ran tick for this second
        if (rem === this._lastTick) return;

        // Save the last tick second
        this._lastTick = rem;

        // Run update every 30 seconds
        if (rem === 30) {
            await this._sendUpdate();
        }
    }

    _boot() {
        let serverPort: number = +(process.env.PORT ?? 9091);
        this._app.listen('127.0.0.1', serverPort, (listenSocket) => {
            if (!listenSocket) return;

            console.log('Listening on http://127.0.0.1:%d/', serverPort);
        });
    }
}

new Server();
