#!/usr/bin/node
//@ts-check

const { createReadStream, existsSync, writeFileSync } = require('fs');
const readline = require('readline');
const { parse } = require('url-otpauth-ng');
const YAML = require('yaml');

const inPath = `${__dirname}/import.txt`;
const outPath = `${__dirname}/config/secrets-imported.yml`;

if (!existsSync(inPath)) {
    console.log('import.txt file does not exist!');
    process.exit(1);
}

const rl = readline.createInterface({
    input: createReadStream(inPath),
    crlfDelay: Infinity
});

(async () => {
    const list = [];

    for await (let line of rl) {
        line = line.trim();

        if (!line.length) continue;

        if (!line.startsWith('otpauth://')) {
            console.log('Skipping "%s" ...', line);
            continue;
        }

        try {
            list.push(
                parse(line)
            );
        } catch (e) {
            console.log('Failed to parse "%s"!', line)
        }
    }

    if (!list.length) {
        console.log('No valid secrets found!');
        return;
    }

    writeFileSync(outPath, YAML.stringify(list));

    console.log('Secrets now available in config/secrets-imported.yml - rename to secrets.yml to use them!');
})();