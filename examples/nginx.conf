server {
    listen       443      ssl http2;
    listen       [::]:443 ssl http2;
    server_name  lemmein.example.com;
    root         /www/lemmein/client/public;

    location / {
        index index.html;
        try_files $uri $uri/ @node;
    }

    location @node {
        proxy_set_header X-Forwarded-For   $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Real-IP         $remote_addr;
        proxy_set_header Host              $http_host;
        proxy_set_header X-SSL-CERT        $ssl_client_escaped_cert;
        proxy_http_version                 1.1;
        proxy_pass                         http://localhost:9091;
        proxy_redirect                     off;
        proxy_buffering                    off;
    }

    ssl_certificate /etc/nginx/ssl/lemmein.example.com/chain.pem;
    ssl_certificate_key /etc/nginx/ssl/lemmein.example.com/key.pem;
    include /etc/nginx/le-options-ssl.conf;
    ssl_dhparam /etc/nginx/le-dhparams-ssl.pem;

    # Client cert auth
    ssl_verify_client on;
    ssl_client_certificate /www/lemmein/server/keys/ca.pem;
}

server {
    listen       80;
    listen       [::]:80;
    server_name  lemmein.example.com;

    return 302 https://$server_name$request_uri;
}
