import type { ConfigFile } from './config.file';

const config: ConfigFile = {};

export default config;
