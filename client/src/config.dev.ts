import type { ConfigFile } from './config.file';

const config: ConfigFile = {
    ws: 'localhost:9095'
};

export default config;
