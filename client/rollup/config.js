import chokidar from 'chokidar';
import { readFile, writeFile } from 'fs/promises';

const production = !process.env.ROLLUP_WATCH;

export default function ({ target, variants, watch }) {
    if (!variants) {
        throw new Error('No variants defined!');
    }

    if (watch) {
        let watcher = chokidar.watch(Object.values(variants), { ignoreInitial: true });
        watcher.on('change', build);
    }

    async function build() {
        console.log('Updating config file ...');

        const type = production ? 'production' : 'development';

        if (!variants.hasOwnProperty(type)) {
            throw new Error(`Oh no: ${type}`);
        }

        const file = variants[type];
        let contents = await readFile(file, 'utf-8');

        await writeFile(target, contents);
    }

    build();
};
