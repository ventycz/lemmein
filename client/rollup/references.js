import { exec } from 'child_process';
import { basename, join as joinPath } from 'path';
import { readFile } from 'fs/promises';

export default function (opts) {
    return {
        name: 'pre-build-references',
        buildStart: async function () {
            const tsConfigContents = await readFile(opts.config);
            const tsConfig = JSON.parse(tsConfigContents);

            if (!tsConfig.references || !tsConfig.references.length) return;

            const references = tsConfig.references;

            for (let reference of references) {
                const ident = basename(reference.path);
                const cwd = joinPath(opts.cwd, reference.path);

                console.log(`Building reference: ${ident} ...`);

                await new Promise((resolve, reject) => {
                    exec(`pnpm run build`, { cwd }, (err, stdout, stderr) => {
                        if (err) return reject(err);

                        resolve({ stdout, stderr });
                    });
                });
            }
        }
    };
}
